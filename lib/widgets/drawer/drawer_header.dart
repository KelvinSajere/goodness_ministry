import 'package:flutter/material.dart';

class CustomDrawerHeader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 2,
      child: Container(
          child: Container(
        child: DrawerHeader(
          margin: EdgeInsets.zero,
          child: Image(
              image: AssetImage('images/logo.jpeg'),
              alignment: Alignment.centerLeft),
        ),
      )),
    );
  }
}
