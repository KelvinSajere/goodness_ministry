import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:goodness_ministries/widgets/sermons/sermon_video.dart';

class SermonScreen extends StatefulWidget {
  @override
  _SermonScreenState createState() => _SermonScreenState();
}

class _SermonScreenState extends State<SermonScreen>
    with AutomaticKeepAliveClientMixin {
  final List<String> _ids = [
    'q5m09rqOoxE',
    'yaTG10YXaRA',
    'BOK8F9caDic',
    'U3Skc4MQlqU',
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      child: SingleChildScrollView(
          padding: EdgeInsets.all(20.00),
          child: ListView.builder(
            shrinkWrap: true,
            itemCount: _ids.length,
            itemBuilder: (context, index) => SermonWidget(
              title: "Promises",
              description: "By Blah blah on Jan 20 2020",
              videoId: _ids[index],
            ),
          )),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
