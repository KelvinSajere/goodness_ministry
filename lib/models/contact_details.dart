class ContactDetails {
  static String reportEmail = Uri(
          scheme: 'mailto',
          path: 'yovwisajere@gmail.com',
          queryParameters: {'subject': 'Bug for goodness Ministry Application'})
      .toString();
  static String churchEmail = Uri(
          scheme: 'mailto',
          path: ' pst.osita@gmail.com',
          queryParameters: {'subject': 'Bug for goodness Ministry Application'})
      .toString();
  static const String churchPhone = 'tel:+1617 206 0929';
  static const String website = 'https://www.goodnessministries.com/';
  static const String facebook = 'https://www.goodnessministries.com/';
  static const String youtube = 'https://www.goodnessministries.com/';
}
