import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AppVersion extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
          child: Align(
              alignment: AlignmentDirectional.bottomStart,
              child: ListView(
                children: <Widget>[
                  Divider(),
                  Padding(
                      padding:
                          EdgeInsetsDirectional.only(start: 10.0, bottom: 10),
                      child: Text("Version 1.0",
                          style: TextStyle(fontWeight: FontWeight.bold)))
                ],
              )),
          color: Color(0xFFebbe35)),
      flex: 1,
    );
  }
}
