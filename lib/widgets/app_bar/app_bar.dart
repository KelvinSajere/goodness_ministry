import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomAppBar extends StatelessWidget with PreferredSizeWidget {
  final String scripture =
      "And we know that all things work together for good to them that love God, to them who are the called according to his purpose."
      " Rom 8:28";
  @override
  final Size preferredSize;

  CustomAppBar({
    Key key,
  })  : preferredSize = Size.fromHeight(50),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
        backgroundColor: Color(0xFFc7343b),
        elevation: 0,
        leading: Row(
          children: <Widget>[
            IconButton(
              icon: Icon(Icons.menu),
              color: Color(0xFFebbe35),
              onPressed: () => Scaffold.of(context).openDrawer(),
            ),
          ],
        ));
  }
}
