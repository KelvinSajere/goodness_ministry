import 'package:flutter/material.dart';

class DailyScriptureScreen extends StatefulWidget {
  @override
  _DailyScriptureScreenState createState() => _DailyScriptureScreenState();
}

class _DailyScriptureScreenState extends State<DailyScriptureScreen> with AutomaticKeepAliveClientMixin{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: CustomAppBar(),
      // drawer: AppDrawer(),
      body: Text("Daily Scripture"),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
