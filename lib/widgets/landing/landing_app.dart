import 'package:flutter/material.dart';
import 'package:goodness_ministries/screens/daily_scripture_screen.dart';
import 'package:goodness_ministries/screens/giving_screen.dart';
import 'package:goodness_ministries/screens/pod_cast_screen.dart';
import 'package:goodness_ministries/screens/sermon_video_screen.dart';
import 'package:goodness_ministries/screens/testimony_screen.dart';
import 'package:goodness_ministries/widgets/app_bar/app_bar.dart';
import 'package:goodness_ministries/widgets/drawer/app_drawer.dart';

class LandingApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => LandingAppState();
}

class LandingAppState extends State<LandingApp> {
  List<Widget> children = [
    SermonScreen(),
    PodCastScreen(),
    DailyScriptureScreen(),
    TestimonyScreen(),
    GivingScreen()
  ];

  PageController pageController;

  int _selectedIndex = 0;

  @override
  void initState() {
    super.initState();
    pageController = PageController();
  }

  @override
  void dispose() {
    pageController.dispose();
    super.dispose();
  }

  _onTapped(int index) {
    pageController.jumpToPage(index);
  }

  void _onPageChanged(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  Future<bool> _shouldPop() async {
    if (_selectedIndex != 0) {
      _onTapped(0);
      return false;
    }
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _shouldPop,
      child: Scaffold(
        appBar: CustomAppBar(),
        drawer: AppDrawer(),
        body: PageView(
            physics: NeverScrollableScrollPhysics(),
            children: children,
            controller: pageController,
            onPageChanged: _onPageChanged),
        bottomNavigationBar: BottomNavigationBar(
          elevation: 0.0,
          onTap: _onTapped,
          currentIndex: _selectedIndex,
          type: BottomNavigationBarType.fixed,
          backgroundColor: Color(0xFFf4f3f3),
          selectedItemColor: Colors.red,
          unselectedItemColor: Color(0xFF25213e),
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: 'Sermons',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.audiotrack),
              label: 'Pod Cast',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.book),
              label: 'Scripture',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.share),
              label: 'Testimony',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.attach_money_sharp),
              label: 'Giving',
            ),
          ],
        ),
      ),
    );
  }
}
