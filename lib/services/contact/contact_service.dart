import 'package:url_launcher/url_launcher.dart';

class ContactService {
  static ContactService _instance;

  ContactService._internal() {
    _instance = this;
  }

  factory ContactService() => _instance ?? ContactService._internal();

  void contact(String contactDetails) {
    launch(contactDetails);
  }
}