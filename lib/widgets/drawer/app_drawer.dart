import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'app_version_drawer.dart';
import 'drawer_header.dart';
import 'drawer_menu.dart';

class AppDrawer extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: [CustomDrawerHeader(), CustomDrawerMenu(), AppVersion()],
      ),
    );
  }
}
