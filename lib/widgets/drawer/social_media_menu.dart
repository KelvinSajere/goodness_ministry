import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:goodness_ministries/services/contact/contact_service.dart';

class SocialMedia extends StatelessWidget {
  final FaIcon icon;
  final String link;
  final ContactService _service = ContactService();

  SocialMedia({Key key, this.icon, this.link}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: icon,
      onPressed: () => {_service.contact(link)},
    );
  }
}
