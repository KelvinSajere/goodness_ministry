import 'package:flutter/material.dart';
import 'package:goodness_ministries/widgets/app_bar/app_bar.dart';
import 'package:goodness_ministries/widgets/drawer/app_drawer.dart';

class GivingScreen extends StatefulWidget {
  @override
  _GivingScreenState createState() => _GivingScreenState();
}

class _GivingScreenState extends State<GivingScreen>
    with AutomaticKeepAliveClientMixin {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(),
      drawer: AppDrawer(),
      backgroundColor: Color(0xFFf4f3f3),
      body: FlatButton(
    child: Text(
    'PUSH',
    style: TextStyle(fontSize: 32.0, color: Colors.white),
    ),
    onPressed: _push,
    ));
  }

  @override
  bool get wantKeepAlive => true;

  void _push() {
    Navigator.of(context).push(MaterialPageRoute(
      // we'll look at ColorDetailPage later
      builder: (context) => Text("Na Me Sabi")
    ));
  }
}
