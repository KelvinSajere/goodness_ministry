import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:goodness_ministries/models/contact_details.dart';
import 'package:goodness_ministries/widgets/drawer/menu.dart';
import 'package:goodness_ministries/widgets/drawer/social_media_menu.dart';

class CustomDrawerMenu extends StatelessWidget {
  final Color fontColor = const Color(0xFF25213e);
  final Color youtubeColor = Color(0xFFFF0000);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 5,
      child: Container(
        color: Color(0xFFebbe35),
        child: ListView(children: [
          Menu(
            title: 'Report a bug',
            leading: Icon(
              Icons.bug_report,
              color: fontColor,
            ),
            contactDetails: ContactDetails.reportEmail,
          ),
          Divider(),
          Menu(
            title: 'Send us an email',
            leading: Icon(
              Icons.alternate_email,
              color: fontColor,
            ),
            contactDetails: ContactDetails.churchEmail,
          ),
          Divider(),
          Menu(
            title: 'Give us a call',
            leading: Icon(
              Icons.phone_callback_sharp,
              color: fontColor,
            ),
            contactDetails: ContactDetails.churchPhone,
          ),
          Divider(),
          Menu(
            title: 'Visit Our Website',
            leading: Icon(
              Icons.language,
              color: fontColor,
            ),
            contactDetails: ContactDetails.website,
          ),
          Divider(),
          Row(
            children: <Widget>[
              SocialMedia(
                icon: FaIcon(
                  FontAwesomeIcons.youtube,
                  color: youtubeColor,
                  size: 25,
                ),
                link:
                    'https://www.youtube.com/channel/UCLxxI_96IlXT_RmMd4MFvUw/featured',
              ),
              SocialMedia(
                icon: FaIcon(
                  FontAwesomeIcons.facebook,
                  color: Color(0xFF3b5998),
                  size: 25,
                ),
                link: 'https://www.facebook.com/goodnessministries',
              )
            ],
          ),
        ]),
      ),
    );
  }
}
