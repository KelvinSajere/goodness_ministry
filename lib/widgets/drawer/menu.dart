import 'package:flutter/material.dart';
import 'package:goodness_ministries/services/contact/contact_service.dart';

class Menu extends StatelessWidget {
  final String title;
  final Icon leading;
  final String contactDetails;
  final ContactService _contactService = ContactService();

  Menu({Key key, this.title, this.leading, this.contactDetails})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(title),
      leading: leading,
      onTap: () {
        _contactService.contact(contactDetails);
      },
    );
  }
}
