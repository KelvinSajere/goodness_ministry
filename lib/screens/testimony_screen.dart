import 'package:flutter/material.dart';

class TestimonyScreen extends StatefulWidget {
  @override
  _TestimonyScreenState createState() => _TestimonyScreenState();
}

class _TestimonyScreenState extends State<TestimonyScreen>
    with AutomaticKeepAliveClientMixin {
  @override
  Widget build(BuildContext context) {
    return Text("TestimonyScreen");
  }

  @override
  bool get wantKeepAlive => true;
}
