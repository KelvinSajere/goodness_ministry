import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

class SermonWidget extends StatefulWidget {
  final String videoId;
  final String title;
  final String description;

  const SermonWidget({Key key, this.videoId, this.title, this.description})
      : super(key: key);

  @override
  _SermonWidgetState createState() => _SermonWidgetState();
}

class _SermonWidgetState extends State<SermonWidget> {
  final List<String> _ids = [
    'q5m09rqOoxE',
    'yaTG10YXaRA',
    'BOK8F9caDic',
    'U3Skc4MQlqU',
  ];


  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.all(8),
        child: Card(
            elevation: 20.00,
            child: Column(
              children: <Widget>[
                YoutubePlayer(
                  controller: YoutubePlayerController(
                      initialVideoId: widget.videoId,
                      flags: YoutubePlayerFlags(
                        controlsVisibleAtStart: true,
                        hideControls: false,
                        autoPlay: false,
                      )),
                  showVideoProgressIndicator: true,
                  progressIndicatorColor: Colors.blue,
                  progressColors: ProgressBarColors(
                      playedColor: Colors.blue, handleColor: Colors.blueAccent),
                ),
                ListTile(
                  leading: Icon(Icons.album),
                  title: Text(widget.title),
                  subtitle: Text(widget.description),
                ),
              ],
            )));
  }
}
