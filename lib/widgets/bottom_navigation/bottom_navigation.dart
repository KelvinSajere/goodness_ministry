import 'package:flutter/material.dart';

class BottomNavigationStatelessWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(child:
      BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
      backgroundColor: Color(0xFFf4f3f3),
        selectedItemColor: Colors.red,
        unselectedItemColor: Color(0xFF25213e),

        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Sermons',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.audiotrack),
            label: 'Pod Cast',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.event),
            label: 'Events',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.share),
            label: 'Testimony',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.attach_money_sharp),
            label: 'Giving',
          ),
        ],
        currentIndex: 1,
      ),
    );
  }
}
